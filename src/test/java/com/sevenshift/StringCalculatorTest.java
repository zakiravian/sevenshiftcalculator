package com.sevenshift;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * StringCalculatorTest.java - Seven Shift Calculator Technical Challenge Test Cases.
 * @author  Zaki Ahmad
 * @version 1.0.0
 */

public class StringCalculatorTest {

    /*
    1.a The numbers in the string are separated by a comma.
    */
    @Test
    public void commaSeparatedStringTestCase() {
        Assert.assertEquals(10, StringCalculator.add("1,2,3,4"));
    }

    /*
    1.b Empty strings should return 0.
    */
    @Test
    public void emptyStringTestCase() {
        Assert.assertEquals(0, StringCalculator.add(""));
    }

    /*
    1.c The return type should be an integer.
    */
    @Test
    public void integerTestCase() {
        Assert.assertEquals(8, StringCalculator.add("8"));
    }

    /*
    1.d Example input: “1,2,5” - expected result: “8”.
    */
    @Test
    public void commaSeparatedStringAdditionTestCase() {
        Assert.assertEquals(8, StringCalculator.add("1,2,5"));
    }


    /*
    2.a Example: “1\n,2,3” - Result: “6”
    */
    @Test
    public void validateDelimitedStringTypeOneAdditionTestCase() {
        Assert.assertEquals(6, StringCalculator.add("1\n,2,3"));
    }

    /*
    2.b Example 2: “1,\n2,4” - Result: “7”
    */
    @Test
    public void validateDelimitedStringTypeTwoAdditionTestCase() {
        Assert.assertEquals(7, StringCalculator.add("1,\n2,4"));
    }


    /*
    3.b custom delimiter
    Example: “//;\n1;3;4” - Result: 8
    */
    @Test
    public void validateCustomDelimiterTestCaseThree() {
        Assert.assertEquals(8, StringCalculator.add("//;\n1;3;4"));
    }

    /*
    3.e.i custom delimiter
    Example: “//$\n1$2$3” - Result: 6
    */
    @Test
    public void validateCustomDelimiterTestCaseFour() {
        Assert.assertEquals(6, StringCalculator.add("//$\n1$2$3"));
    }

    /*
   3.e.ii custom delimiter
   Example: “//@\n2@3@8” - Result: 13
   */
    @Test
    public void validateCustomDelimiterTestCaseFive() {
        Assert.assertEquals(13, StringCalculator.add("//@\n2@3@8"));
    }

    /*
   4. Calling add with a negative number should throw an exception: “Negatives not allowed”.
   Example: “-1” - Result: Negatives not allowed [-1]
   */
    @Test
    public void validateSingleNegativeValueTestCase() {
        try {
            StringCalculator.add("-1");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Negatives not allowed [-1]", e.getMessage());
        }
    }

    /*
   4. Calling add with a negative number should throw an exception: “Negatives not allowed”.
   Example: “-1, -2” - Result: Negatives not allowed [-1, -2]
   */
    @Test
    public void validateMultiNegativeValueTestCase() {
        try {
            StringCalculator.add("-1,-2");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Negatives not allowed [-1, -2]", e.getMessage());
        }
    }

    /*
    Bonus
    Numbers larger than 1000 should be ignored.
    Example “2,1001” - Result: 2
    */
    @Test
    public void ignoreGreaterThanThousand() {

        Assert.assertEquals(2, StringCalculator.add("2,1001"));
    }

    /*
    Bonus
     // Allow for multiple delimiters
     Example a. “//$,@\n1$2@3” - Result 6
    */
    @Test
    public void validateMultiDelimeter() {
        Assert.assertEquals(6, StringCalculator.add("//$,@\n1$2@3"));
    }

    /*
    Bonus
     // Allow for multiple delimiters
     Example a. “//$,@@@$$$\n1@2$3” - Result 6
    */
    @Test
    public void validateMultiDelimeterLongLength() {
        Assert.assertEquals(6, StringCalculator.add("//$,@@@$$$\n1@2$3"));
    }


    /*
    Bonus
     // Delimiters with arbitrary length
     Example a. “//***\n1***2***3” - Result 6
    */
    @Test
    public void validateArbitraryLength() {

    }

}
