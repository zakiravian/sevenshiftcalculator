package com.sevenshift;


import java.util.ArrayList;
import java.util.List;

/**
 * StringCalculator.java - A class to demonstrate the use of String Integer in calculator.
 * @author  Zaki Ahmad
 * @version 1.0.0
 */

public class StringCalculator {

    /**
     * Creating inner class to avoid creating instance in TestCase Class
     * @return InnerCalculator add function.
     */

    private final String input;
    private String delimiter = "[,\n]";
    private String numbersWithDelimiters;
    private List<String> negatives = new ArrayList<String>();

    // overload constructor
    private StringCalculator(String input) {
        this.input = input;
    }

    /**
     * perform addition function.
     * @return perform addition and return response in integer format.
     */
    public static int add(String input) {
        InnerCalculator stringCalculator = new InnerCalculator(input);
        return stringCalculator.add();
    }

    // Static InnerCalculator Class
    static class InnerCalculator {

        private final String input;
        private String delimiter = "[,\n]";
        private String numbersWithDelimiters;
        private List<String> negatives = new ArrayList<String>();

        // constructor
        private InnerCalculator(String input) {
            this.input = input;
        }

        /**
         * parse string into double.
         * @return boolean (true/false).
         */
        private boolean isNumeric(String str) {
            try {
                Double.parseDouble(str);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }

        /**
         * perform addition
         * @return calculation sum.
         */
        public int add() {
            if (isStringEmpty()) {
                return 0;
            }
            // parse delimeter, scan through numbers and calculate sum
            parseDelimiterAndFindLineWithNumbersAndDelimiters();
            return calculateSum(splitInputByDelimiter());
        }

        /**
         * check if input string is empty
         */
        private boolean isStringEmpty() {
            return input.length() == 0;
        }

        /**
         * parse delimeter, find numbers in input string and populate number with delimeters string
         */
        private void parseDelimiterAndFindLineWithNumbersAndDelimiters() {
            if (isCustomDelimiter()) {
                parseDelimiter();
                findLineWithNumbersAndDelimiters();
            } else {
                numbersWithDelimiters = input;
            }
        }

        /**
         * check if input string starts with custom delimeter
         * @return boolean (true,false)
         */
        private boolean isCustomDelimiter() {
            return input.startsWith("//");
        }

        private void parseDelimiter() {
            delimiter = "";
            addDelimiters();
        }

        /**
         * scan through input string and add delimter
         * */
        private void addDelimiters() {

            if(!input.contains("[")) {
                // if input string doesn't contain brackets let's check if its multidelimeter or single
                String delimeterType[] = input.split("\\n");
                if(delimeterType[0].length() > 3) {
                    // its multidelimeter
                    delimiter += delimeterType[0].substring(2);
                } else {
                    // single delimeter
                    delimiter += input.substring(2,3);
                }
            } else {
                int startIndexDelimiter = 0;
                while (true) {
                    startIndexDelimiter = input.indexOf("[", startIndexDelimiter) + 1;
                    if (startIndexDelimiter == 0) {
                        break;
                    }
                    int endIndexDelimiter = input.indexOf("]", startIndexDelimiter);
                    delimiter += input.substring(startIndexDelimiter, endIndexDelimiter);
                }
            }
        }


        private void findLineWithNumbersAndDelimiters() {
            int firstNewLine = input.indexOf("\n");
            if(firstNewLine > 3) {
                // its multiDelimeter
                numbersWithDelimiters = input.substring(firstNewLine + 1).replaceAll("[$@]", "@");
            } else {
                numbersWithDelimiters = input.substring(firstNewLine + 1);
            }


        }

        /**
         * split input string by delimeter
         * @return string array to perform sum
         */
        private String[] splitInputByDelimiter() {

            if(delimiter.length() > 1 && (!delimiter.contains("[")) ) {
                // convert multidelimeter string to single delimeter
                delimiter = "@";
            }
            // if delimiter contains special character
            if(delimiter.equalsIgnoreCase("$")) {
                delimiter = "\\"+delimiter;
            }
            if(delimiter.contains("\\s*")) {
                // Metacharacters
            }
            return numbersWithDelimiters.split(delimiter);
        }

        /**
         * calcul
         * @return string array to perform sum
         */
        private int calculateSum(String[] inputSplittedByDelimiter) {
            int result = 0;
            for (String character : inputSplittedByDelimiter) {
                if(character.isEmpty()) {
                } else {
                    result += addSingleCharacter(character);
                }
            }
            throwExceptionIfNegativeCharactersExist();
            return result;
        }


        /**
         * throw illegal argument exception if any negative character(s) exist.
         * */
        private void throwExceptionIfNegativeCharactersExist() {
            if (hasNegativeTokens()) {
                throw new IllegalArgumentException(String.format("Negatives not allowed "+ negatives));
            }
        }

        /**
         * check if negatives arraylist contains any value
         * @return boolean (true/false).
         */
        private boolean hasNegativeTokens() {
            return negatives.size() > 0;
        }

        /**
         * parse character, check if value is negative or is in valid range
         * @return int.
         */
        private int addSingleCharacter(String character) {
            Integer valueAsInteger = Integer.parseInt(character);
            if (isNegative(valueAsInteger)) {
                negatives.add(character);
            } else if (isInValidRange(valueAsInteger)) {
                return valueAsInteger;
            }
            return 0;
        }

        /**
         * check if input values is negative.
         * @return boolean type.
         */
        private boolean isNegative(Integer valueAsInteger) {
            return valueAsInteger < 0;
        }

        /**
         * validate integer range.
         * @return an integer data type.
         */
        private boolean isInValidRange(Integer valueAsInteger) {
            return valueAsInteger < 1000;
        }

    }

}