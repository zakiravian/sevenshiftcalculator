# Seven Shift String Calculator

## Compile

Run `gradle build`

## Test

Run `gradle test`

Then, after executing the tests, you can watch the test result on [test report page](./build/reports/tests/test/index.html).
